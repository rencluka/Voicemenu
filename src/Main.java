import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import com.sun.tools.internal.ws.wsdl.document.jaxws.Exception;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Main {


    public static class Event{
        private String name;
        private String trigger;
        private List <Event> childs;
        private String action;
        private String toast;

        public String getName() {
            return name;
        }

        public String getTrigger() {
            return trigger;
        }
    }

    public static class Number{
        private static Integer value;

        public static void setValue(Integer value) {
            Number.value = value;
        }
    }

    public static class CellPhone {

        private String path = "Home";
        final private String NEWLINE ="\n";

        Event myEvent = new Event();
        public static HashMap myHashMap = new HashMap();

        private JPanel myPanelOfButtons;
        private JPanel myPhone;
        private JFrame myFrame;
        private JTextArea myScreen;

        private JButton Button_1;
        private JButton Button_2;
        private JButton Button_3;
        private JButton Button_4;
        private JButton Button_5;
        private JButton Button_6;
        private JButton Button_7;
        private JButton Button_8;
        private JButton Button_9;
        private JButton Button_plus;
        private JButton Button_0;
        private JButton Button_hash;


        public CellPhone() {
            init();
        }
        private void initHashmap(){
            Number myNumber = new Number();
            myNumber.setValue(1);

            Event myEvent = new Event();
            myEvent.action = "Internet";
            myHashMap.put()
        }
        private void buttonPressed(){
            //voice initialization
            VoiceManager vm = VoiceManager.getInstance();
            Voice myVoice = vm.getVoice("kevin16");
            myVoice.allocate();
            final String[] returnText = new String[1];

            Button_1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_1.getText());
//                    myScreen.append(Button_1.getText()+ NEWLINE + INDENT);
//                    INDENT = INDENT + " ";

                    path = path + "->" + Button_1.getText();
                    myScreen.append(path + NEWLINE);
                    }
            });

            Button_2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_2.getText());
//                    myScreen.append(Button_2.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_2.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_3.getText());
//                    myScreen.append(Button_3.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_3.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_4.getText());
//                    myScreen.append(Button_4.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_4.getText();
                    myScreen.append(path + NEWLINE);

                }
            });

            Button_5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_5.getText());
//                    myScreen.append(Button_5.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_5.getText();
                    myScreen.append(path + NEWLINE);

                }
            });

            Button_6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_6.getText());
//                    myScreen.append(Button_6.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_6.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_7.getText());
//                    myScreen.append(Button_7.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_7.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_8.getText());
//                    myScreen.append(Button_8.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_8.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_9.getText());
//                    myScreen.append(Button_9.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_9.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_plus.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_plus.getText());
//                    myScreen.append(Button_plus.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_plus.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_0.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_0.getText());
//                    myScreen.append(Button_0.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";
                    path = path + "->" + Button_0.getText();
                    myScreen.append(path + NEWLINE);
                }
            });

            Button_hash.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    myVoice.speak(Button_hash.getText()+ "going back to the previous menu.");
//                    myScreen.append(Button_hash.getText() + NEWLINE + INDENT);
//                    INDENT = INDENT + " ";


//                    My special Action go back
                    if(path.length() > 0){
                        path = path.substring(0,path.length()-3);
                    }
                   // path = path + "->" + Button_hash.getText();
                    myScreen.append(path + NEWLINE);
                }
            });
            try {
                Thread.sleep(5000);

            }catch (InterruptedException e){}

        }

        private void init(){
            myPhone = new JPanel(new BorderLayout(5,5));
            myPhone.setBorder(new EmptyBorder(4,4,4,4));

            myScreen = new JTextArea("",20,8);
            myScreen.append("MENU:\n");

            myPhone.add(myScreen, BorderLayout.NORTH);

            myFrame = new JFrame("jetPhone");
            myFrame.setContentPane(myPhone);
            myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            myPanelOfButtons = new JPanel(new GridLayout(4,3,2,2));

            Button_1 = new JButton("1");
            myPanelOfButtons.add(Button_1);

            Button_2 = new JButton("2");
            myPanelOfButtons.add(Button_2);

            Button_3 = new JButton("3");
            myPanelOfButtons.add(Button_3);

            Button_4 = new JButton("4");
            myPanelOfButtons.add(Button_4);

            Button_5 = new JButton("5");
            myPanelOfButtons.add(Button_5);

            Button_6 = new JButton("6");
            myPanelOfButtons.add(Button_6);

            Button_7 = new JButton("7");
            myPanelOfButtons.add(Button_7);

            Button_8 = new JButton("8");
            myPanelOfButtons.add(Button_8);

            Button_9 = new JButton("9");
            myPanelOfButtons.add(Button_9);

            Button_plus = new JButton("+");
            myPanelOfButtons.add(Button_plus);

            Button_0 = new JButton("0");
            myPanelOfButtons.add(Button_0);

            Button_hash = new JButton("#");
            myPanelOfButtons.add(Button_hash);

            myPhone.add(myPanelOfButtons,BorderLayout.CENTER);
            myFrame.pack();
            myFrame.setVisible(true);



        }

    }



    public static void main(String[] args) {

        CellPhone mpsPhone = new CellPhone();

        mpsPhone.buttonPressed();


    }
}

